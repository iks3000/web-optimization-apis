import { hamburger } from "./hamburger";
import { events, changeContentStandard, changeContentAdvance, remove } from "./join-us-section";
import './web-component';
import { sendClickData } from './send-click-data';
import './performance-api';
import { year } from './full-year';

hamburger();
events();
changeContentStandard();
// changeContentAdvance();
// remove();
sendClickData();
year();
