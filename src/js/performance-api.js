import { getUser } from './get-users';

const urlPerf = '/analytics/performance';
const dataTiming = {};

const t0_LoadFetch = performance.now();
getUser();
const t1_LoadFetch = performance.now();
const resultTimingFecth = t1_LoadFetch - t0_LoadFetch;

dataTiming["Measure the performance of fetching"] = `${ resultTimingFecth } ms`;

const t0_LoadPage = performance.now();
window.addEventListener("load", (() => {
    console.log("Page Lodaed!");
})());
const t1_LoadPage = performance.now();

const resultTimingLoadPage = t1_LoadPage - t0_LoadPage;

dataTiming["Page load speed"] = `${ resultTimingLoadPage } ms`;
dataTiming["Page memory usage"] = `${ performance.memory.usedJSHeapSize / Math.pow(1000, 2) } MB`;

fetch(urlPerf, {
    method: "POST",
    headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
    body: JSON.stringify(dataTiming),
})
    .then(response => {
        if (response.ok === true) {
            return response.json();
        }
    })
    .then((data) => {
        console.log('Success:', data);
    })
    .catch((error) => {
        console.error('Error:', error);
    });

console.log(dataTiming)
